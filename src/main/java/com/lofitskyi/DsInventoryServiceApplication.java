package com.lofitskyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class DsInventoryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsInventoryServiceApplication.class, args);
	}
}
